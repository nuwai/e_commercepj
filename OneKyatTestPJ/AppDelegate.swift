//
//  AppDelegate.swift
//  OneKyatTestPJ
//
//  Created by BaganIT on 1/28/22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        configureUIApperance()
        configureRootController()
        return true
    }
    



}

extension AppDelegate {
    
    fileprivate func configureUIApperance(){
        
        UINavigationBar.appearance().barTintColor = .green

        
        UINavigationBar.appearance().tintColor = .white

        
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor : UIColor.white,
                                                            .font : UIFont.systemFont(ofSize: 20)]
        
        UINavigationBar.appearance().isTranslucent = false
    }
    
    fileprivate func configureRootController(){
        
        var isLogInFinish :Bool!
        if let checkValue = UserDefaults.standard.object(forKey: "isLogIn") as? Bool {
            isLogInFinish = checkValue
        }
        else {
            isLogInFinish = false
        }
        let tabBarModule = TabBarRouter.createModule()
        let logInModule = LogInRouter.createModule()
        if isLogInFinish {
            window?.rootViewController = tabBarModule
        }
        else {
            window?.rootViewController = logInModule
        }
       
        window?.makeKeyAndVisible()
    }
}

