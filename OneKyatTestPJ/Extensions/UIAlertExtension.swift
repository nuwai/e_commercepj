//
//  UIAlertExtension.swift
//  OneKyatTestPJ
//
//  Created by BaganIT on 1/29/22.
//

import Foundation
import UIKit

extension UIViewController {

  func presentAlert(withTitle title: String, message : String) {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let OKAction = UIAlertAction(title: "OK", style: .default) { action in
    }
    alertController.addAction(OKAction)
    if let topVC = UIApplication.getTopViewController() {
       topVC.present(alertController, animated: true, completion: nil)
    }
    
  }
}

extension UIApplication {

    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {

         
        if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}
