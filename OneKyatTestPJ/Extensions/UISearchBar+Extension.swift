//
//  UISearchBar+Extension.swift
//  OneKyatTestPJ
//
//  Created by BaganIT on 1/29/22.
//

import UIKit

extension UISearchBar {
    
    func customSearchBar() {
        setImage(UIImage(named: "search_box"), for: .search, state: .normal)
        backgroundImage = UIImage()
        
        if let textfield = value(forKey: "searchField") as? UITextField {
            textfield.layer.cornerRadius = 10
            textfield.layer.borderWidth = 1
            textfield.layer.borderColor = UIColor.clear.cgColor
            textfield.backgroundColor = .white
            textfield.placeholder = "Search at Onekyat"
            textfield.attributedPlaceholder = NSAttributedString(string: textfield.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : UIColor.green,NSAttributedString.Key.font:UIFont.systemFont(ofSize: 14)])
        }
    }
    
}

