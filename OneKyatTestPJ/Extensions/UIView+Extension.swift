//
//  UIView+Extension.swift
//  OneKyatTestPJ
//
//  Created by BaganIT on 1/29/22.
//

import Foundation
import UIKit

extension UIView {
    
   
    func setupCardView(color: UIColor = .white, radius: CGFloat = 4) {
        backgroundColor = color
        layer.cornerRadius = radius
        layer.shadowColor = UIColor.gray.withAlphaComponent(0.8).cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        layer.shadowOpacity = 0.8  //0.8
        layer.shadowRadius = 2
    }
    
   
    
}
