//
//  HomeInteractor.swift
//  OneKyatTestPJ
//
//  Created by BaganIT on 1/28/22.
//

import Foundation

class HomeInteractor: PresenterToInteractorHomeProtocol {
    
    // MARK: - Protocols
    
    var presenter: InteractorToPresenterHomeProtocol?
    
    // MARK: - functions
    
    func getSliders() {
       let bannerImages =  ["banner-01","banner-02","banner-03","banner-04","banner-05","banner-06","banner-07"]
        self.presenter?.getSliderDatas(with: bannerImages)
    }
    
    func getProductDatas() {
        var data = [[String:String]]()
        let productImages =  [["name":"carImage-1",
                               "image":"carImage-1","price":"300,000Lakhs"],["name":"carImage-2","image":"carImage-2","price":"300,000Lakhs"],["name":"carImage-3",
                                                                                                           "image":"carImage-3","price":"300,000Lakhs"],["name":"carImage-4",
                                                                                                                                                 "image":"carImage-4","price":"300,000Lakhs"]]
        
        for _ in 0...199 {
            let randomData =  productImages.randomElement()
            data.append(randomData!)
        }
       
        self.presenter?.getProductDatas(with: data)
    }
    
    
    
}
