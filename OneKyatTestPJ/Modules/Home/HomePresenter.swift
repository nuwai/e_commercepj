//
//  HomePresenter.swift
//  OneKyatTestPJ
//
//  Created by BaganIT on 1/28/22.
//

import Foundation
import UIKit

class HomePresenter: ViewToPresenterHomeProtocol {
    
    
    
    // MARK: - Protocols
    
    var view: PresenterToViewHomeProtocol?
    var interactor: PresenterToInteractorHomeProtocol?
    var router: PresenterToRouterHomeProtocol?
    
    // MARK: Private variables
    var sliders: [String]?
    fileprivate var bannerADS: [[String:String]] = [[:]]
    
    // MARK: - functions
    
    func viewDidLoad() {
        
        interactor?.getSliders()
        interactor?.getProductDatas()
        view?.loadData()
      
    }
    
   
    func numberOfSection() -> Int {
        return 1
    }
    
    func numberOfItemsIn(section: Int) -> Int {
        
         return  self.bannerADS.count
        
    }
    
    func cellForItemAt(index: Int) -> [String:String]{
        return bannerADS[index]
    }
    
    func didSelectRowAt(index: Int) {
        router?.pushToProductScreen(on: view!, with: bannerADS[index])
    }
    
    func goLogIn(){
        router?.pushToLogInScreen(on: view!)
    }
}

// MARK: - InteractorToPresenterHomeProtocol

extension HomePresenter: InteractorToPresenterHomeProtocol {
    
    func getSliderDatas(with data: [String]) {
        self.sliders = data
    }
    
    func getProductDatas(with data: [[String:String]]) {
        self.bannerADS = data
    }
    
}

