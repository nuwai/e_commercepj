//
//  HomeProtocol.swift
//  OneKyatTestPJ
//
//  Created by BaganIT on 1/28/22.
//

import Foundation
import UIKit

// MARK: - ViewToPresenterHomeProtocol

protocol ViewToPresenterHomeProtocol {
    var view: PresenterToViewHomeProtocol? { get set }
    var interactor: PresenterToInteractorHomeProtocol? { get set }
    var router: PresenterToRouterHomeProtocol? { get set }
    
    var sliders: [String]? { get set }
    
    func viewDidLoad()
    
    func numberOfSection() -> Int
    func numberOfItemsIn(section: Int) -> Int
    func cellForItemAt(index: Int) -> [String:String]
    func didSelectRowAt(index: Int)
    func goLogIn()
    
}

// MARK: - PresenterToViewHomeProtocol

protocol PresenterToViewHomeProtocol {
    
    func loadData()
   
}

// MARK: - PresenterToRouterHomeProtocol

protocol PresenterToRouterHomeProtocol {
    static func createModule() -> HomeViewController
    func pushToProductScreen(on view: PresenterToViewHomeProtocol, with productData: [String:String])
    func pushToLogInScreen(on view: PresenterToViewHomeProtocol)
}

// MARK: - PresenterToInteractorHomeProtocol

protocol PresenterToInteractorHomeProtocol {
    var presenter: InteractorToPresenterHomeProtocol? { get set }
    func getSliders()
    func getProductDatas()
   
}

// MARK: - InteractorToPresenterHomeProtocol

protocol InteractorToPresenterHomeProtocol {
    
    func getSliderDatas(with data: [String])
    func getProductDatas(with data: [[String:String]])
    
}
