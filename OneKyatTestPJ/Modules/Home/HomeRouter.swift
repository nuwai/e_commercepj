//
//  HomeRouter.swift
//  OneKyatTestPJ
//
//  Created by BaganIT on 1/28/22.
//

import Foundation
import UIKit

class HomeRouter: PresenterToRouterHomeProtocol {
    
    // MARK: - Static functions
    
    static func createModule() -> HomeViewController {
        
        let view = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeViewController

        var presenter: ViewToPresenterHomeProtocol & InteractorToPresenterHomeProtocol = HomePresenter()
        var interactor: PresenterToInteractorHomeProtocol = HomeInteractor()
        let router: PresenterToRouterHomeProtocol = HomeRouter()

        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
    }
    
    static var storyboard: UIStoryboard {
        return UIStoryboard(name: "Home", bundle: nil)
    }
    
    // MARK: - functions
    
    func pushToProductScreen(on view: PresenterToViewHomeProtocol, with productData: [String:String]) {
        let sourceVC = view as! HomeViewController
        let destinationVC = ProductDetailRouter.createModule(with: productData)
        destinationVC.modalPresentationStyle = .fullScreen
        sourceVC.present(destinationVC, animated: true, completion: nil)
    }
    
    func pushToLogInScreen(on view: PresenterToViewHomeProtocol) {
        let sourceVC = view as! HomeViewController
        let destinationVC = LogInRouter.createModule()
        destinationVC.modalPresentationStyle = .fullScreen
        sourceVC.present(destinationVC, animated: true, completion: nil)
    }
    
   
    
}
