//
//  HomeViewController.swift
//  OneKyatTestPJ
//
//  Created by BaganIT on 1/28/22.
//

import UIKit

class HomeViewController: UIViewController {
    
    // MARK: - Protocol
    
    var presenter: ViewToPresenterHomeProtocol?
    
    var searchBar = UISearchBar()
    
    // MARK: - Outlets
    
    @IBOutlet weak var mainCollectionView: UICollectionView!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavigationSearchBar()
        presenter?.viewDidLoad()
        
    }
    

   

}

// MARK: - PresenterToViewHomeProtocol

extension HomeViewController: PresenterToViewHomeProtocol {
    
    func loadData() {
        mainCollectionView.reloadData()
    }
    
}

// MARK: - UICollectionViewDataSource
    
extension HomeViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return presenter?.numberOfSection() ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter?.numberOfItemsIn(section: section) ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HomeHeaderCollectionReusableView", for: indexPath) as! HomeHeaderView
           
            
           
            headerView.sliderData = presenter?.sliders
            
            return headerView
        default:
            fatalError()
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productCell", for: indexPath) as! productCell
        if let product = presenter?.cellForItemAt(index: indexPath.row){
            cell.productImage.image = UIImage(named: product["image"] ?? "")
            cell.titleLB.text = product["name"] ?? ""
            cell.priceLB.text = product["price"] ?? ""
        }
        
        
        
        return cell
    }
    
    
    
}

// MARK: - UICollectionViewDelegate

extension HomeViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return  CGSize(width: collectionView.frame.width, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter?.didSelectRowAt(index: indexPath.row)
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (collectionView.frame.width - 24)/2
        let height = (width * 4)/3
        return  CGSize(width: width, height: height)
    }
    
}

// MARK: - Private functions

extension HomeViewController {
    
    fileprivate func configureNavigationSearchBar() {

        searchBar.customSearchBar()
        navigationItem.titleView = searchBar
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "logOut"), style: .done, target: self, action: #selector(logOut))
       
    }
    
    @objc func logOut(){
        UserDefaults.standard.removeObject(forKey: "isLogIn")
        presenter?.goLogIn()
    }
    
}


