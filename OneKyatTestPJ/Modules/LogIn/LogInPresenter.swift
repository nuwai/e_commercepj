//
//  LogInPresenter.swift
//  OneKyatTestPJ
//
//  Created by BaganIT on 1/28/22.
//

import Foundation
import UIKit

class LogInPresenter: ViewToPresenterLogInProtocol {
    
    // MARK: - Protocols
    
    var view: PresenterToViewLogInProtocol?
    var interactor: PresenterToInteractorLogInProtocol?
    var router: PresenterToRouterLogInProtocol?
    
    
    // MARK: - functions
    
    func viewDidLoad() {
        
    }
    
    func showEmptyMessageAlert(){
        view?.showAlert()
    }
    
    func checkUser(){
        view?.checkUserPhoneAndPassword(userPhone: "09420000001", userPassword: "password_123$#")
    }
    
    func successLogIn(){
        //saveLogIn
        UserDefaults.standard.setValue(true, forKey: "isLogIn")
        
        router?.pushToHomeScreen(on: view!)
    }
    
   
    
}

// MARK: - InteractorToPresenterLogInProtocol

extension LogInPresenter: InteractorToPresenterLogInProtocol {
    
   
    
}
