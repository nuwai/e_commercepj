//
//  LogInProtocol.swift
//  OneKyatTestPJ
//
//  Created by BaganIT on 1/28/22.
//

import Foundation
import UIKit

// MARK: - ViewToPresenterLogInProtocol

protocol ViewToPresenterLogInProtocol {
    var view: PresenterToViewLogInProtocol? { get set }
    var interactor: PresenterToInteractorLogInProtocol? { get set }
    var router: PresenterToRouterLogInProtocol? { get set }
    
    
    func viewDidLoad()
    func showEmptyMessageAlert()
    func checkUser()
    func successLogIn()
    
}

// MARK: - PresenterToViewLogInProtocol

protocol PresenterToViewLogInProtocol {

    func showAlert()
    func checkUserPhoneAndPassword(userPhone:String,userPassword:String)
   
}

// MARK: - PresenterToRouterLogInProtocol

protocol PresenterToRouterLogInProtocol {
    static func createModule() -> LogInViewController
    func pushToHomeScreen(on view: PresenterToViewLogInProtocol)
}

// MARK: - PresenterToInteractorLogInProtocol

protocol PresenterToInteractorLogInProtocol {
    var presenter: InteractorToPresenterLogInProtocol? { get set }
   
}

// MARK: - InteractorToPresenterLogInProtocol

protocol InteractorToPresenterLogInProtocol {
    
}
