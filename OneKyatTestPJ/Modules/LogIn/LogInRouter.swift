//
//  LogInRouter.swift
//  OneKyatTestPJ
//
//  Created by BaganIT on 1/28/22.
//

import Foundation
import UIKit

class LogInRouter: PresenterToRouterLogInProtocol {
    
    // MARK: - Static functions
    
    static func createModule() -> LogInViewController {
        
        let view = storyboard.instantiateViewController(withIdentifier: "LogInVC") as! LogInViewController

        var presenter: ViewToPresenterLogInProtocol & InteractorToPresenterLogInProtocol = LogInPresenter()
        var interactor: PresenterToInteractorLogInProtocol = LogInInteractor()
        let router: PresenterToRouterLogInProtocol = LogInRouter()

        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
    }
    
    static var storyboard: UIStoryboard {
        return UIStoryboard(name: "LogIn", bundle: nil)
    }
    
    func pushToHomeScreen(on view: PresenterToViewLogInProtocol) {
        
        let sourceVC = view as! LogInViewController
        let destinationVC = TabBarRouter.createModule()
        destinationVC.modalPresentationStyle = .fullScreen
        sourceVC.present(destinationVC, animated: true, completion: nil)
    }
    
    
    
   
    
}
