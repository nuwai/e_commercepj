//
//  LogInViewController.swift
//  OneKyatTestPJ
//
//  Created by BaganIT on 1/28/22.
//

import UIKit

class LogInViewController: UIViewController {

    // MARK: - Protocol
    
    var presenter: ViewToPresenterLogInProtocol?
    
    // MARK: - Outlets
    
    
    
    @IBOutlet weak var phoneTF: UITextField!
    
    @IBOutlet weak var passwordTF: UITextField!
    
    @IBOutlet weak var loginBtn: UIButton!
    
    @IBOutlet weak var checkTCBtn: UIButton!
    
    @IBOutlet weak var checkTCImageView: UIImageView!
    
    var isCheckTC :Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        setUpTextFieldDelegate()
        setUpTC()
        presenter?.viewDidLoad()
        
    }
    
    // MARK: - Actions

    @IBAction func loginAction(_ sender: Any) {
        
        self.checkEmpty()
    }
    
    @IBAction func tcAction(_ sender: Any) {
        
        guard let url = URL(string: "https://www.onekyat.com/") else {
          return
        }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    // MARK: - Functions
    
    func setUpTextFieldDelegate(){
        self.phoneTF.delegate = self
        self.phoneTF.keyboardType = .numberPad
        self.passwordTF.delegate = self
    }
    
    func checkEmpty(){
        
        if self.phoneTF.text!.isEmpty || self.passwordTF.text!.isEmpty {
            presenter?.showEmptyMessageAlert()
        }
        else {
            presenter?.checkUser()
        }
        
    }
    
    
    @IBAction func tcCheckAction(_ sender: Any) {
        
        if isCheckTC {
            self.checkTCImageView.image = UIImage(named: "uncheck")
            self.isCheckTC = false
            self.loginBtn.isEnabled = false
        }
        else {
            self.checkTCImageView.image = UIImage(named: "check")
            self.isCheckTC = true
            if  !self.phoneTF.text!.isEmpty && !self.passwordTF.text!.isEmpty {
                self.loginBtn.isEnabled = true
            }
            else {
                self.loginBtn.isEnabled = false
            }
           
        }
        
    }
    
}

// MARK: - PresenterToViewLogInProtocol

extension LogInViewController: PresenterToViewLogInProtocol {
    
    
    func showAlert(){
        self.presentAlert(withTitle: "Error", message: "Need to fill phone and password.")
    }
    
    func checkUserPhoneAndPassword(userPhone:String,userPassword:String){
        
        if isCheckTC {
       
            if let phone = self.phoneTF.text, let password = self.passwordTF.text {
                if isValidPhone(phoneString: phone){
                if phone != userPhone || password != userPassword {
                    self.presentAlert(withTitle: "Log In Error", message: "Don't match Phone and Password.")
                }
                else {
                    presenter?.successLogIn()
                }
            }
                else {
                    self.presentAlert(withTitle: "Error", message: "Invalid Phone Number.")
                }
        }
            else {
                self.presentAlert(withTitle: "Error", message: "Need to fill phone and password.")
            }
        }
        else {
            self.presentAlert(withTitle: "", message: "Need to agreed our Terms and Condition.")
        }
    }
    
   
}

// MARK: - UITesxtFieldDelegate

extension LogInViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("END")
        if !self.phoneTF.text!.isEmpty && !self.passwordTF.text!.isEmpty && isCheckTC{
            self.loginBtn.isEnabled = true
        }
        else {
            self.loginBtn.isEnabled = false
        }
        return
    }
}

// MARK: - Private functions
extension LogInViewController {
    
    func setUpTC(){
        
        self.checkTCImageView.image = UIImage(named: "uncheck")
        self.isCheckTC = false
    }
    
}



