//
//  ProductDetailPresenter.swift
//  OneKyatTestPJ
//
//  Created by BaganIT on 1/28/22.
//

import Foundation
import UIKit

class ProductDetailPresenter: ViewToPresenterProductDetailProtocol {
    
    
    
    // MARK: - Protocols
    
    var view: PresenterToViewProductDetailProtocol?
    var interactor: PresenterToInteractorProductDetailProtocol?
    var router: PresenterToRouterProductDetailProtocol?
    
    // MARK: Private variables
    var passProductData:[String:String]?
    
    // MARK: - functions
    
    func viewDidLoad() {
        print(passProductData)
        view?.setUpPassData(data: passProductData!)
      
    }
    
   
   
}

// MARK: - InteractorToPresenterHomeProtocol

extension ProductDetailPresenter: InteractorToPresenterProductDetailProtocol {
    
   
    
}


