//
//  ProductDetailProtocol.swift
//  OneKyatTestPJ
//
//  Created by BaganIT on 1/28/22.
//

import Foundation
import UIKit

// MARK: - ViewToPresenterProductDetailProtocol

protocol ViewToPresenterProductDetailProtocol {
    
    var view: PresenterToViewProductDetailProtocol? { get set }
    var interactor: PresenterToInteractorProductDetailProtocol? { get set }
    var router: PresenterToRouterProductDetailProtocol? { get set }
    
    var passProductData:[String:String]?{ get set }
   
    
    func viewDidLoad()
    
   
}

// MARK: - PresenterToViewProductDetailProtocol

protocol PresenterToViewProductDetailProtocol {
    
    func setUpPassData(data:[String:String])
   
}

// MARK: - PresenterToRouterProductDetailProtocol

protocol PresenterToRouterProductDetailProtocol {
    static func createModule(with data:[String:String]) -> ProductDetailViewController
}

// MARK: - PresenterToInteractorProductDetailProtocol

protocol PresenterToInteractorProductDetailProtocol {
    var presenter: InteractorToPresenterProductDetailProtocol? { get set }
    
   
}

// MARK: - InteractorToPresenterProductDetailProtocol

protocol InteractorToPresenterProductDetailProtocol {
    
    
    
}
