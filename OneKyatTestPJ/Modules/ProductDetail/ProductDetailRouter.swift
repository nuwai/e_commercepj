//
//  ProductDetailRouter.swift
//  OneKyatTestPJ
//
//  Created by BaganIT on 1/28/22.
//

import Foundation
import UIKit

class ProductDetailRouter: PresenterToRouterProductDetailProtocol {
    
    // MARK: - Static functions
    
    static func createModule(with data:[String:String]) -> ProductDetailViewController {
        
        let view = storyboard.instantiateViewController(withIdentifier: "ProductDetailVC") as! ProductDetailViewController

        var presenter: ViewToPresenterProductDetailProtocol & InteractorToPresenterProductDetailProtocol = ProductDetailPresenter()
        var interactor: PresenterToInteractorProductDetailProtocol = ProductDetailInteractor()
        let router: PresenterToRouterProductDetailProtocol = ProductDetailRouter()

        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        presenter.passProductData = data
        interactor.presenter = presenter
        
        return view
    }
    
    static var storyboard: UIStoryboard {
        return UIStoryboard(name: "Home", bundle: nil)
    }
    
    
   
    
}

