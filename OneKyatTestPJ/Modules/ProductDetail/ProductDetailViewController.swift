//
//  ProductDetailViewController.swift
//  OneKyatTestPJ
//
//  Created by BaganIT on 1/28/22.
//

import UIKit

class ProductDetailViewController: UIViewController {
    
    // MARK: - Protocol
    
    var presenter: ViewToPresenterProductDetailProtocol?

    // MARK: - Outlets
    
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var mainImageVIew: UIImageView!
    
    @IBOutlet weak var nameLB: UILabel!
    
    @IBOutlet weak var priceLB: UILabel!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter?.viewDidLoad()
        
    }
    
    // MARK: - Actions
    
    @IBAction func dismissViewController(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func phoneCallBtnAction(_ sender: Any) {
        guard let number = URL(string: "tel://" + "+959420000001") else { return }
        UIApplication.shared.open(number)
    }
}

// MARK: - PresenterToViewProductDetailProtocol

extension ProductDetailViewController: PresenterToViewProductDetailProtocol {
    
    func setUpPassData(data:[String:String]){
        
        print(data)
        let image = data["image"]
        self.mainImageVIew.image = UIImage(named: image ?? "")
        let name = data["name"]
        self.nameLB.text = name
        let price = data["price"]
        self.priceLB.text = price
    }
    
    
}
