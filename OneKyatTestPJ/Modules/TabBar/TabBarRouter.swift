//
//  TabBarRouter.swift
//  OneKyatTestPJ
//
//  Created by BaganIT on 1/28/22.
//

import UIKit

struct TabBarRouter {
    
    // MARK: - Static functions
    
    static func createModule() -> UITabBarController {
        let tabBarController = storyboard.instantiateViewController(withIdentifier: "tabBar") as! UITabBarController
        
        let nav1 = UINavigationController(rootViewController: HomeRouter.createModule())
        let nav2 = UINavigationController(rootViewController: HomeRouter.createModule())
        let nav3 = UINavigationController(rootViewController: HomeRouter.createModule())
        let nav4 = UINavigationController(rootViewController: HomeRouter.createModule())
        let nav5 = UINavigationController(rootViewController: HomeRouter.createModule())
        
        
        nav1.tabBarItem = UITabBarItem(title: "Home", image: UIImage(named: "home"), tag: 0)
        nav2.tabBarItem = UITabBarItem(title: "Message", image: UIImage(named: "message"), tag: 1)
        nav3.tabBarItem = UITabBarItem(title: "Notification", image: UIImage(named: "notification"), tag: 2)
       nav4.tabBarItem = UITabBarItem(title: "Favourite", image: UIImage(named: "favourite"), tag: 3)
        nav5.tabBarItem = UITabBarItem(title: "Profile", image: UIImage(named: "profile"), tag: 4)
       
        
        tabBarController.viewControllers = [nav1,nav2,nav3,nav4,nav5]
        
        return tabBarController
    }
    
    static var storyboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
    
}

