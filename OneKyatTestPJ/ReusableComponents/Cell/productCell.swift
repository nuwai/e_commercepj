//
//  productCell.swift
//  OneKyatTestPJ
//
//  Created by BaganIT on 1/28/22.
//

import UIKit

class productCell: UICollectionViewCell {
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var priceLB: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()
    }
    
    func configureView(){
        bgView.setupCardView()
    }
    
}
