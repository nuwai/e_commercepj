//
//  HomeHeaderView.swift
//  OneKyatTestPJ
//
//  Created by BaganIT on 1/28/22.
//

import UIKit
import FSPagerView


class HomeHeaderView: UICollectionReusableView {
    
    // MARK: - Public variables
    
    public var sliderData: [String]? {
        didSet {
            pagerView.reloadData()
        }
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            pagerView.itemSize = .zero
            pagerView.isInfinite = true
            pagerView.automaticSlidingInterval = 5.0
        }
    }
    @IBOutlet var pageControl: FSPageControl! {
        didSet {
            pageControl.contentHorizontalAlignment = .center
            pageControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
            pageControl.setFillColor(.lightGray, for: .normal)
            pageControl.setFillColor(.green, for: .selected)
        }
    }
   
    // MARK: - Override functions
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
}


// MARK: - FSPagerViewDataSource, FSPagerViewDelegate

extension HomeHeaderView: FSPagerViewDataSource, FSPagerViewDelegate {
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return sliderData?.count ?? 0
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        
        guard let imageName = sliderData?[index] else { return cell }
        cell.imageView?.image = UIImage(named: imageName)
        cell.imageView?.contentMode = .scaleToFill
        
        pageControl.numberOfPages = sliderData?.count ?? 0
        
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        pageControl.currentPage = index
    }
    
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        guard pageControl.currentPage != pagerView.currentIndex else { return }
        pageControl.currentPage = pagerView.currentIndex
    }
    
}

